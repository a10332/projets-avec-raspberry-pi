# Projets avec Raspberry Pi
Petits projets simples et utiles avec le RPI Modèle B Rev2.

## 1. Mesurer le niveau de CO2 avec le MH-Z14A
Dans ce dossier, il y a un script python pour [lire le niveau de CO2 d'un capteur MH-Z14A et l'enregistrer dans un fichier](source/mesurer_co2/mh-z14a_mesurer_co2.py). On trouve aussi une photo qui montre les connexions au Raspberry Pi Modèle B Rev2.

### 1.1. Pièces nécessaires
1. RPI: facile à trouver sur internet.
2. MH-Z14A: j'ai préféré l'acheter ici [MH-Z14A au Abra Électronique Montréal](https://abra-electronics.com/sensors/sensors-gas/sens-mh-z14a.html?currency=CAD), mais il y en a à d'autres endroits.
3. Pour le sertissage des fils, j'ai utilisé [un kit comme celui-ci](https://abra-electronics.com/interconnects/connectors/headers/kits/con-kit-003-dupont-connector-housing-kit-620pcs.html).

### 1.2. Configuration du RPI
1. La plus récente version de raspbian. Idéalement sans desktop graphique si on utilise un vieux RPI comme le mien. J'ai utilisé une version de raspbian sortie après septembre 2022.
2. Activer le port sériel UART avec la commande en terminal `raspi-config`. Il faut suivre les menus et trouver l'option.
3. Installer `pip3` avec `sudo apt-get install pip3`.
4. Installer `pyserial` avec `pip install pyserial`.

### 1.3 Photo des connexions
![connexions_rpi_mh-z14a.jpg](suppléments/mesurer_co2/connexions_rpi_mh-z14a.jpg)

## 2. Mesurer la qualité de l'air avec le SGP30
Le script python pour lire des mesures de qualité de l'air se trouve dans [sgp30_mesurer_qualite_air.py](source/sgp30_qualité_air/sgp30_mesurer_qualite_air.py).
Les images de connexions se trouvent dans [suppléments/sgp30_qualité_air](suppléments/sgp30_qualité_air).

## 2.1. Pièces nécessaires
1. Un RPI comme d'habitude.
2. Un SGP30, préférablement [la version d'Adafruit](https://www.adafruit.com/product/3709) avec le système [Qwiic de Sparkfun](https://www.sparkfun.com/qwiic). J'ai trouvé le mien au [Abra Électronique](https://abra-electronics.com/sensors/sensors-gas/3709-ada-adafruit-sgp30-air-quality-sensor-breakout-voc-and-eco2.html).
3. Un câble Qwiic déjà serti avec des connecteurs DuPont pour se connecter directement aux broches du RPI. On peut s'en fabriquer un avec les pièces suivantes: [kit de connecteurs DuPont](https://abra-electronics.com/interconnects/connectors/headers/kits/con-kit-003-dupont-connector-housing-kit-620pcs.html), [câble Qwiic](https://abra-electronics.com/interconnects/connectors/qwiic/flexible-qwiic-cable-50mm.html), une [pince de sertissage similaire à celles qu'on voit ici](https://cours.polymtl.ca/inf1900/guides/guideMontage/) plusieurs tailles existent, faites vos recherches.

## 2.2. Configuration du RPI
1. Activer la communication I2C avec `sudo raspi-config` (choisir l'option `3 Interface Options` et puis `I5 I2C`).
2. Installer `smbus2` avec `pip install smbus2` (plus de [détails sur smbus2 ici](https://pypi.org/project/smbus2/)). Sans cette librairie, j'avais une exception au lancement de l'instanciation de `SGP30` dans mon script python.
3. Installer la plus récente version de la [librairie de pimoroni sgp30-python](https://github.com/pimoroni/sgp30-python). J'ai installé la dernière version de développement comme décrit dans la page git de la librairie. Voici les mêmes étapes recopiées ici: `git clone https://github.com/pimoroni/sgp30-python`, `cd sgp30-python`, `sudo ./install.sh --unstable`. Il y aura un script `test.py` à `/home/nom_utilisateur/Pimoroni/pimoroni-sgp30/examples/`. On le lance avec `python3 test.py` et des valeurs devraient s'afficher après le réchauffement du capteur.

## 2.3 Photo des connexions
L'Adafruit SGP30 avec son câble Qwiic:
![sgp30_serti.jpg](suppléments/sgp30_qualité_air/sgp30_serti.jpg)

Les broches du RPI Modèle B Rev2 avec le MH-Z14A sur la rangée du bas et le SGP30 sur la rangée du haut:
![connexions_rpi_sgp30.jpg](suppléments/sgp30_qualité_air/connexions_rpi_sgp30.jpg)

## 2.4 Plus de détails
1. Quel signal passe sur quel fil du câble Qwiic? [Voir sur le site de Sparkfun dans la section «What’s the pinout again?»](https://cdn.sparkfun.com/assets/custom_pages/2/7/2/QwiicPinoutGraphic.jpg). Lien direct vers l'image du site: ![QwiicPinoutGraphic.jpg](https://cdn.sparkfun.com/assets/custom_pages/2/7/2/QwiicPinoutGraphic.jpg)
2. Quel broche du RPI utiliser? Plusieurs images se trouvent sur internet, en voici une que j'ai consulté: ![HD-pinout-of-R-Pi-3-Model-B-GPIO-scaled.jpg](https://www.etechnophiles.com/wp-content/uploads/2020/12/HD-pinout-of-R-Pi-3-Model-B-GPIO-scaled.jpg)

