"""
Ce programme utilise un Raspberry Pi modèle B rev2 avec un capteur MH-Z14A pour lire la concentration de CO2
d'une pièce. La communication fonctionne en UART. Il faut activer le port sériel en utilisant raspi-config et
redémarrer le RPI avant d'utiliser ce script. Il faut aussi sudo apt-get install pip3 et pip install pyserial
pour que le import serial fonctionne.

À NOTER QUE LORSQU'ON PART LE SCRIPT ENVIRON DANS LES 5 PREMIÈRES MINUTES APRÈS LE DÉMARRAGE DU RPI, LES
VALEURS LUES PEUVENT ÊTRE FAUSSES. ELLES SERONT DANS LES 30 000 OU LES 50 000. IL SUFFIT DE REDÉMARRER LE
SCRIPT POUR CORRIGER CE PROBLÈME.

Copyright (C) 2022 Alpaca Balena.

Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence
Publique Générale GNU telle que publiée par la Free Software Foundation ; soit la version 3 de cette
licence, soit (à vous de voir...) toute autre version ultérieure.

Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE, ni explicite ni
implicite, y compris les garanties de commercialisation ou d'adaptation dans un but spécifique. Prenez
connaissance de la Licence Publique Générale GNU pour plus de détails.

Vous devez avoir reçu une copie de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas le
cas, voir <https://www.gnu.org/licenses/>.
"""
from datetime import datetime, timedelta
import time
import serial


DEMANDE_NIVEAU_CO2 = [0xff, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79]


def enregistrer_niveau_co2():
    """
    Enregistrer le niveau de CO2.

    1. On affiche le niveau de CO2 à l'écran.
    2. On l'enregistre dans un fichier.

    Notes:
    a. Le port UART utilisé est celui d'un RPI Modèle B roulant la plus récente version de raspbian aux
    alentours de novembre 2022.
    b. On demande une nouvelle valeur du capteur MH-Z14A à chaque 4 secondes en dormant.
    """
    port_seriel_uart = "/dev/ttyAMA0"
    vitesse_de_communication_en_baud = 9600
    canal_uart = serial.Serial(
        port=port_seriel_uart,
        baudrate=vitesse_de_communication_en_baud,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1,
    )

    while True:
        aujourdhui = datetime.now()
        interval_de_jours_de_donnees = 1
        demain = aujourdhui + timedelta(days=interval_de_jours_de_donnees)
        with open(
            f"{aujourdhui.strftime('%Y-%m-%d.')}niveaux_CO2.csv",
            "a+",
            encoding="utf-8",
        ) as enregistrement:
            maintenant = datetime.now()
            while maintenant.date() < demain.date():
                canal_uart.write(bytearray(DEMANDE_NIVEAU_CO2))
                reponse = canal_uart.read(9)
                if len(reponse) == 9:
                    maintenant = datetime.now()
                    ppm_de_co2 = (reponse[2] << 8) | reponse[3]
                    enregistrement.write(f"{maintenant.isoformat()}, {ppm_de_co2}, {reponse[4]}\n")
                    # On «flush» pour s'assurer que la ligne soit écrite dans le fichier aussitôt que
                    # possible.
                    enregistrement.flush()
                    # On «flush» pour que le terminal se mette à jour tout de suite avec la nouvelle valeur.
                    print(ppm_de_co2, end=", ", flush=True)
                else:
                    print(f"La longueur de la réponse n'était pas 9. Réponse: {reponse}.")
                secondes_dattente_avant_prochaine_demande = 4
                time.sleep(secondes_dattente_avant_prochaine_demande)


if __name__ == "__main__":
    enregistrer_niveau_co2()

