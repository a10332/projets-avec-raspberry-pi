"""
Ce programme utilise un Raspberry Pi modèle B rev2 avec un capteur Adafruit SGP30. Ce capteur a une précision
de 15% pour mesurer le taux total de composés organiques volatiles en parties par milliard. Il retourne aussi
une valeur équivalente de CO2 dans l'air en partie par million. Ce capteur n'est pas un «vrai» capteur de CO2
comme le MH-Z14A, mais il semble suivre relativement bien l'augmentation ou la diminution du CO2 d'une pièce.

AU PREMIER DÉMARRAGE, IL EST POSSIBLE QUE LES VALEURS LUES SOIENT DE 400 ET 0 POUR UN LONG MOMENT. APRÈS
QUELQUES MINUTES OU QUELQUES HEURES, LES MESURES SEMBLENT MEILLEURES.

Copyright (C) 2022 Alpaca Balena.

Ce programme est libre, vous pouvez le redistribuer et/ou le modifier selon les termes de la Licence
Publique Générale GNU telle que publiée par la Free Software Foundation ; soit la version 3 de cette
licence, soit (à vous de voir...) toute autre version ultérieure.

Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE, ni explicite ni
implicite, y compris les garanties de commercialisation ou d'adaptation dans un but spécifique. Prenez
connaissance de la Licence Publique Générale GNU pour plus de détails.

Vous devez avoir reçu une copie de la Licence Publique Générale GNU avec ce programme ; si ce n'est pas le
cas, voir <https://www.gnu.org/licenses/>.
"""
from datetime import datetime, timedelta
from sgp30 import SGP30
import time


def imprimer_barre_progression():
    """Imprimer une barre de progression."""
    print("▮", end="", flush=True)


def mesurer_qualite_air():
    """Mesurer la qualité de l'air."""
    sgp30 = SGP30()

    print("Le capteur se réchauffe: ", end="", flush=True)
    sgp30.start_measurement(imprimer_barre_progression)

    while True:
        aujourdhui = datetime.now()
        interval_de_jours_de_donnees = 1
        demain = aujourdhui + timedelta(days=interval_de_jours_de_donnees)
        with open(
            f"{aujourdhui.strftime('%Y-%m-%d.')}sgp30_qualité_de_lair.csv",
            "a+",
            encoding="utf-8",
        ) as enregistrement:
            maintenant = datetime.now()
            mesure = sgp30.get_air_quality()
            enregistrement.write(f"{maintenant.isoformat()}, {mesure.equivalent_co2}, {mesure.total_voc}\n")
            print(f"(co2: {mesure.equivalent_co2}, voc: {mesure.total_voc})", end=", ", flush=True)
            secondes_dattente_avant_prochaine_mesure = 4.0
            time.sleep(secondes_dattente_avant_prochaine_mesure)


if __name__ == "__main__":
    mesurer_qualite_air()

